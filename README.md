# README #

### Popcorn ML code for Python3 Jupyter notebook ###

This is the offline model generation part

### Require exact version of these libraries ###
Keras: `1.2.2`
Tensorflow: `1.4.0` (No GPU needed)

### Other libraries can be installed without specifying version ###
